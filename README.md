spades-assembly-3.13.0-gf
============

Version: 0.1

This GeneFlow app wraps the spades assembly tool. It takes forward and reverse reads and generates an assembly from them. 

Inputs
------
1. input: Forward FASTQ file.
2. pair: Reverse FASTQ file.

Parameters
----------
1. output: spades output directory.

